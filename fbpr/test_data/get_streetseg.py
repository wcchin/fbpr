# -*- coding: UTF-8 -*-
import random
from numpy import median
import pandas as pd
import geopandas as gpd
import matplotlib.pyplot as plt
from shapely.geometry import Point


def plot_lines(ax, obs):
    for ob in obs:
        x, y = ob.xy
        ax.plot(x, y, color=GRAY, linewidth=1.5, solid_capstyle='round', zorder=1)

def plot_line(ax, ob, col=None):
    if col is None:
        col = GRAY
    x, y = ob.xy
    ax.plot(x, y, color=col, linewidth=1.5, solid_capstyle='round', zorder=1)

def get_intersections(oo,gg):
    # this part is for finding the intersections and export to csv
    intersect = []
    for i in range(len(oo)):
        k = oo[i]
        g = gg[i]
        for j in range(i+1,len(oo),1):
            k2 = oo[j]
            g2 = gg[j]
            if k!=k2:
                inter = g.intersection(g2)
                if isinstance(inter, Point):
                    intersect.append((k,k2))
                else:
                    if not(len(list(inter))==0):
                        print inter
        #break
    print len(intersect)

    inters = {}
    i = 0
    for u,v in intersect:
        inters[i] = dict(n1=u,n2=v)
        i+=1
    df = pd.DataFrame.from_dict(inters, orient='index')
    print df
    df.to_csv('inter_TP_1m.csv', index=False)


def get_medianpoint(oo,gg):
    #this part is for getting the median point of segments
    mpoint = {}
    for k,g in zip(oo,gg):
        #print len(g.xy)
        xx,yy = g.xy
        #print xx,yy
        xm = median(xx)
        ym = median(yy)
        mpoint[k] = dict(xm=xm, ym=ym)
        #break
    df2 = pd.DataFrame.from_dict(mpoint, orient='index')
    print df2.head()
    df2.to_csv('mpoint_tp_1m.csv', index_label='rid')

def draw_map():

    inters = pd.read_csv('inter_TP_1m.csv')
    mpoints = pd.read_csv('mpoint_tp_1m.csv')
    print inters.head()
    print mpoints.head()

    fig, ax = plt.subplots()
    ax.set_aspect('equal')
    plot_lines(ax, street.geometry)

    rs = mpoints.rid.tolist()
    xs = mpoints.xm.tolist()
    ys = mpoints.ym.tolist()
    #ax.scatter(xs,ys)

    mps = { k:(x,y) for k,x,y in zip(rs,xs,ys) }
    n1s = inters.n1.tolist()
    n2s = inters.n2.tolist()
    for n1,n2 in zip(n1s,n2s):
        x1,y1 = mps[n1]
        x2,y2 = mps[n2]
        ax.plot([x1,x2],[y1,y2])

    plt.show()

def rand_flow(oo, perc=0.1):
    k = int(len(oo)*perc)
    samp = random.sample(oo, k)
    flows = {}
    for p in samp:
        f = random.uniform(1.,100.)
        flows[p] = {'flow':f}
    df3 = pd.DataFrame.from_dict(flows, orient='index')
    #print df3
    df3.to_csv('flow_TP_1m.csv', index_label='rid')

if __name__ == '__main__':

    BLUE = '#6699cc'
    GRAY = '#999999'

    street = gpd.read_file('squaremiles_tp_street.shp')
    #print street.head()

    oo = street['ROADID']
    print len(oo) == len(list(set(oo)))
    print len(oo)
    gg = street['geometry']

    #get_medianpoint(oo,gg)
    #get_intersections(oo,gg)
    #draw_map()
    rand_flow(oo,perc=0.1)
