from fbpr import FBPR

intersf = 'inter_TP_1m.csv'
flowsf = 'flow_TP_1m.csv'
inters_fs = { 'node1':'n1', 'node2':'n2' }
flows_fs = {'node':'rid', 'volume':'flow'}
fbpr = FBPR(intersf, flowsf, inters_fs=inters_fs, flows_fs=flows_fs)
#fbpr.init_solutions(pool_size=3, set_count=3)
#fbpr.run_all(max_generation=20, max_PRiter=500, run_pool=True, save_iter=False)
fbpr.get_optimal_sol(export=True)
fbpr.ga_performance(set_no=0)
fbpr.ga_performance(set_no=1)
fbpr.ga_performance(set_no=2)
