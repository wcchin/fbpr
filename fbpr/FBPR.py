# -*- coding: UTF-8 -*-

import os

import pandas as pd

import data_import
import init_solution as initing
import ga_optimize as ga
import ga_performance as perform_check
import optimal_sol as optimal

class FBPR(object):
    def __init__(self, intersf, flowsf, inters_fs=None, flows_fs=None, working_dir='working', volrange=(1,100)):
        if working_dir[-1]=='/':
            working_dir = working_dir[:-1]
        self.volrange = volrange
        self.flowsfile = flowsf
        self.flows_filesetup = flows_fs
        self.working_dir = working_dir
        self.fbobj = data_import.FBobj(intersf, flowsf, inters_fs=inters_fs, flows_fs=flows_fs)

    def init_solutions(self, pool_size=50, set_count=100, genr_expect=1000):
        solution_dir = self.working_dir+'/solutions'
        sol_len = self.fbobj.ag.number_of_nodes()
        initing.initial_solutions(pool_size=pool_size, sol_len=sol_len, set_count=set_count, volrange=self.volrange, solution_dir=solution_dir, genr_expect=genr_expect)

    def get_setz(self):
        solution_dir = self.working_dir+'/solutions'
        dirs =list(sorted( os.listdir(solution_dir) ))
        dirs = [ d for d in dirs if d[0:3]=='set' ]
        adir = dirs[0]
        set_z = len(adir[4:])
        return set_z

    def run_all(self, max_generation=500, max_PRiter=500, iscontinue=False, run_pool=False, save_iter=False):
        solution_dir = self.working_dir+'/solutions'
        dirs =list(sorted( os.listdir(solution_dir) ))
        for d in dirs:
            set_dir = solution_dir+'/'+d+'/'
            #print set_dir
            ga.ga_operation(self.fbobj, set_dir, max_PRiter=max_PRiter, max_generation=max_generation, volrange=self.volrange, iscontinue=iscontinue, run_pool=run_pool, save_iter=save_iter)
            #break

    def run_set(self, set_no, max_generation=500, max_PRiter=500, iscontinue=False, save_iter=False, run_pool=False):
        d = 'set_'+str(set_no).zfill(self.get_setz())
        set_dir = self.working_dir+'/solutions/'+d+'/'
        ga.ga_operation(self.fbobj, set_dir, max_PRiter=max_PRiter, max_generation=max_generation, volrange=self.volrange, iscontinue=iscontinue, run_pool=run_pool, save_iter=save_iter)

    def ga_performance(self, set_no):
        d = 'set_'+str(set_no).zfill(self.get_setz())
        set_dir = self.working_dir+'/solutions/'+d+'/'
        perform_check.plot_result(set_dir)

    def get_optimal_sol(self, export=True):
        solution_dir = self.working_dir+'/solutions'
        #dirs =list(sorted( os.listdir(solution_dir) ))
        best_sol = optimal.get_optimal(solution_dir)

        nodes_list = self.fbobj.nodes_list
        best_dict = {}
        for n in nodes_list:
            i = nodes_list.index(n)
            best_dict[n] = {'attractiveness': best_sol[i]}
        best_df = pd.DataFrame.from_dict(best_dict, orient='index')
        if export:
            fname = solution_dir+'/output/optimal_solution.csv'
            dirname = os.path.dirname(fname)
            if not os.path.exists(dirname):
                os.makedirs(dirname)
            best_df.to_csv(fname)
        return best_df

if __name__ == '__main__':
    intersf = '../test_data/inter_TP_1m.csv'
    flowsf = '../test_data/flow_TP_1m.csv'
    inters_fs = { 'node1':'n1', 'node2':'n2' }
    flows_fs = {'node':'rid', 'volume':'flow'}
    fbpr = FBPR(intersf, flowsf, inters_fs=inters_fs, flows_fs=flows_fs)
    #fbpr.init_solutions(pool_size=3, set_count=3)
    #fbpr.run_all(max_generation=20, max_PRiter=500, run_pool=True, save_iter=False)
    #fbpr.get_optimal_sol()
    fbpr.ga_performance(set_no=0)
