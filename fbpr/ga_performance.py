# -*- coding: UTF-8 -*-

import pandas as pd
import os
import matplotlib.pyplot as plt
import matplotlib as mpl
import seaborn as sns

def plot_result(set_dir):
    #setno2 = str(setno).zfill(3)
    #%matplotlib inline
    res_path = set_dir+'fitness/'
    files = os.listdir(res_path)
    rhos = []
    for f in sorted(files):
        #print f
        if f[-4:]=='.csv':
            temp = pd.read_csv(res_path+f)
            #print temp.head()
            temp_rhos = temp.rho.tolist()
            rhos.append(sorted(temp_rhos))
            #print len(temp)

    norm = mpl.colors.Normalize(vmin=0, vmax=len(rhos))
    #norm(0)
    cmap = mpl.cm.get_cmap('YlGnBu')
    #rgba = cmap(0.5)
    fig,(ax,ax3) = plt.subplots(1,2, sharey=True)
    #fig.patch.set_facecolor('black')
    #sns.set_style(style="dark")
    for i in range(len(rhos)):
        l = rhos[i]
        ax.plot(l, c=cmap(norm(i)))

    ax.set_title('solutions rho of all generation')
    ax.set_xlabel('chromosome (sorted)')
    ax.set_ylabel('rho')

    """
    norm2 = mpl.colors.Normalize(vmin=0, vmax=50)
    cmap2 = mpl.cm.get_cmap('gray_r')
    fig2,ax2 = plt.subplots()
    improved = []
    z = zip(rhos)
    for j in range(len(z)):
        #print s[0]
        s = z[j][0]
        temp = []
        for i in range(len(s)-1):
            pre = s[i]
            post = s[i+1]
            temp.append(post-pre)
        improved.append(temp)
        #print temp
        #generations = [i for i in range(len(temp))]
        #ax2.scatter(generations,temp, c=cmap2(norm(j)))
        ax2.plot(temp, c=cmap2(norm2(j)))
    """

    #fig3,ax3 = plt.subplots()
    best_improved = []
    for l in rhos:
        j = len(l)-1
        last = l[j]
        best_improved.append(last)
    ax3.plot(best_improved)
    ax3.set_title('best rho in each generation')
    ax3.set_xlabel('generation')
    #plt.show()
    set_name = res_path.split("/")[-3]
    res_root = '/'.join(res_path.split("/")[:-3])
    output_path = res_root+'/output/'+set_name+'_fitness.png'
    plt.savefig(output_path, dpi=300)
    plt.close()
