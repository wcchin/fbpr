# -*- coding: UTF-8 -*-

import os
import copy

import pagerank as PR
import fitness_evaluate as fitness
import ga_process as GA

class ga_operation(object):
    def __init__(self, fbobj, set_dir, max_generation=500, iscontinue=True, start_generation=0, max_PRiter=500, volrange=(1,100), run_pool=False, save_iter=False):
        sol_list = list(sorted(os.listdir(set_dir+'/sols')))
        form = sol_list[0]
        self.genr_expect_len = len(form[5:-4])
        self.set_name = set_dir.split("/")[1]
        self.set_dir = set_dir
        self.fbobj = fbobj
        self.volrange = volrange

        if iscontinue:
            last_gnr_sol = sol_list[-1]
            #print sol_list
            last_gnr = int(last_gnr_sol[5:-4])
            this_generation = last_gnr
        else:
            this_generation = start_generation
        self.pool_size = len(self.get_sols(this_generation))
        self.runFBPR(max_generation, this_generation, max_PRiter, run_pool, save_iter)

    def get_sols(self, generation):
        gnz = str(generation).zfill(self.genr_expect_len)
        fname = self.set_dir+'/sols/gnrt_'+gnz+'.csv'
        f = open(fname, 'rb')
        lines = f.readlines()
        f.close()
        return lines

    def get_sol(self, generation, gene_no):
        lines = self.get_sols(generation)
        agene0 = lines[gene_no].split(',')
        agene = [int(ch) for ch in agene0]
        #print len(agene)
        return agene

    def set_attrs(self, ag, attrlist=None):
        nodelist = self.fbobj.nodes_list
        if attrlist is None:
            for n in ag.nodes_iter():
                ag.node[n]['attr'] = 1
                ag.node[n]['out_sum'] = 0
        else:
            for n in ag.nodes_iter():
                i = nodelist.index(n)
                ag.node[n]['attr'] = attrlist[i]
                ag.node[n]['out_sum'] = 0
        self.get_link_weight(ag)
        return ag

    def get_link_weight(self, ag):
        for n in ag.nodes_iter():
            out = ag[n]
            #print n, out
            suma = 0
            for d in out:
                suma = suma + ag.node[d]['attr']
            ag.node[n]['out_sum'] = suma

        for o,d in ag.edges_iter():
            da = ag.node[d]['attr']
            os = ag.node[o]['out_sum']
            #ag.node[o]['out_sum'] = ag.node[o]['out_sum'] + da
            ag.edge[o][d]['link_weight'] = float(da)/float(os)
            #print ag.edge[o][d]['link_weight']
            #print ag.edge[o][d]['link_weight']<=1

    def runFBPR(self, max_generation, this_generation, max_PRiter, run_pool, save_iter):

        base = dict(set_dir=self.set_dir, genr_expect_len=self.genr_expect_len, max_PRiter=max_PRiter, pool_size=self.pool_size, save_iter=save_iter)
        #flowsfile = self.fbobj.flowsfile
        #flows_filesetup = self.fbobj.flows_filesetup
        while (this_generation<max_generation):
            print 'running ',self.set_name,': generation: ',this_generation, ' / ', max_generation
            ag = copy.deepcopy(self.fbobj.ag)
            flows_df = self.fbobj.flows
            PR.run_a_generation(ga_op=self, base=base, ag=ag, generation=this_generation, pool_size=self.pool_size, run_pool=run_pool)
            fitness.get_fitness_ofa_generation(generation=this_generation, set_dir=self.set_dir, flows_df=flows_df, genr_expect_len=self.genr_expect_len)

            GA.process_GA(ga_op=self, generation=this_generation, set_dir=self.set_dir, genr_expect_len=self.genr_expect_len, volrange=self.volrange)
            this_generation = this_generation + 1
