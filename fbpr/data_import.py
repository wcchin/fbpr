# -*- coding: UTF-8 -*-

import pandas as pd

import get_network as getnet
#import fitness_evaluate as fiteval

class FBobj(object):
    def __init__(self, intersf, flowsf, inters_fs=None, flows_fs=None):
        self.links = getnet.file_read(intersf, inters_fs=inters_fs)
        self.flows = self.get_flow(flowsf, flows_fs=flows_fs)
        ag, nl = getnet.create_graph(self.links)
        self.ag = ag
        self.nodes_list = nl
        #print self.flows.head()

    def get_flow(self, flowsf, flows_fs=None):
        if flows_fs is None:
            flows_fs = { 'node':'node', 'volume':'flows' }
        df_vol = pd.read_csv(flowsf)
        df_vol = df_vol.rename(columns={ v:k for k,v in flows_fs.iteritems() })
        df_vol.set_index('node', inplace=True)
        return df_vol
