# -*- coding: UTF-8 -*-
import os
import csv
import numpy as np

def init_one_solutions(pool_size, sol_len, volrange=(1,100)):
    sol_no = pool_size # number of chromosomes(solutions) in one generation
    each_sol = sol_len # the length of each chromosome(solution)
    v1,v2 = volrange
    sols0 = list(np.random.randint(v1,v2,size=(sol_no,each_sol)))
    ## attractiveness is in range 1~100
    sols = [list(s) for s in sols0]
    return sols

def check_dir_exist(dirname):
    if not os.path.isdir(dirname):
        os.makedirs(dirname)

def initial_solutions(pool_size, sol_len, set_count=100, volrange=(1,100), solution_dir='working/solutions', genr_expect=1000):
    #dirname = os.path.dirname(solution_dir)
    #check_dir_exist(dirname)
    set_len = len(str(set_count))
    for i in range(set_count): # 100 sets of initial generation
        sols = init_one_solutions(pool_size=pool_size, sol_len=sol_len, volrange=volrange)
        sz = str(i).zfill(set_len)
        gz = str(0).zfill(len(str(genr_expect)))

        filename = solution_dir+'/'+'set_'+sz+'/sols/gnrt_'+gz+'.csv'
        dirname = os.path.dirname(filename)
        check_dir_exist(dirname)
        with open(filename, 'wb') as f:
            writer = csv.writer(f)
            for l in sols:
                writer.writerow(l)
        print 'solution created:', sz
