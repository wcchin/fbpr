# -*- coding: UTF-8 -*-

import os

import pandas as pd

def get_optimal(solution_dir):
    set_dirs = list(sorted( os.listdir(solution_dir) ))
    max_rho = 0
    max_path = None
    max_chrom = None
    set_dirs = [ d for d in set_dirs if d[0:3]=='set' ]
    for sdir in set_dirs:
        fitdir = solution_dir+'/'+sdir+'/fitness'
        soldir = solution_dir+'/'+sdir+'/sols'
        last_gnr = list(sorted(os.listdir(fitdir)))[-1]
        last_path = fitdir+'/'+last_gnr
        last_path2 = soldir+'/'+last_gnr
        df_gnr = pd.read_csv(last_path)
        #print df_gnr
        #df_gnr['rho'].tolist()
        max_row = df_gnr.loc[df_gnr['rho'].idxmax()]
        #print max_row['rho']
        if max_row['rho']>max_rho:
            max_rho = max_row['rho']
            max_path = last_path2
            max_chrom = int(max_row['gene_no'])
            #print last_path, max_row['gene_no']
        #break
    best_sol = get_op_sol(last_path2, max_chrom)
    #print best_sol
    return best_sol

def get_op_sol(last_path2, max_chrom):
    f = open(last_path2, 'rb')
    lines = f.readlines()
    f.close()
    agene0 = lines[max_chrom].split(',')
    #print agene0
    agene = [int(ch) for ch in agene0]
    return agene
