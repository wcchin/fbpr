# -*- coding: UTF-8 -*-

import os
import multiprocessing
import copy
import csv
import pandas as pd

def start_process():
    print 'Starting',multiprocessing.current_process().name

def initialize_PR(ag):
    evenscore = 1.0/float(ag.number_of_nodes())
    for n in ag.nodes_iter():
        ag.node[n]['score'] = evenscore
        ag.node[n]['new_score'] = 0.0
        ag.node[n]['equi'] = False
    return ag

def run_PR(ag, set_dir, generation, gn, genr_expect_len, max_PRiter, pool_size, save_iter=False):
    iter_len = len(str(max_PRiter))
    i = 0
    while (i<max_PRiter):
        run_iter(ag)
        if save_iter:
            save_pr_iter(ag, i, set_dir, generation, gn, pool_size, max_PRiter, genr_expect_len)
        i = i + 1
    save_pr(ag, set_dir, generation, gn, pool_size, genr_expect_len)
    st = set_dir.split('/')[2]
    print 'runned: set: '+str(st)+', gn:'+ str(gn)

def run_iter(ag):
    for o,d in ag.edges_iter():
        ops = ag.node[o]['score']
        ew = ag.edge[o][d]['link_weight']
        flow = ops * ew
        ag.node[d]['new_score'] = ag.node[d]['new_score'] + flow

    for n in ag.nodes_iter():
        ag.node[n]['score'] = ag.node[n]['new_score']
        ag.node[n]['new_score'] = 0.0

def save_pr_iter(ag, runned, set_dir, generation, gn, pool_size, max_PRiter, genr_expect_len):
    zpool = len(str(pool_size))
    ziter = len(str(max_PRiter))
    zgnrt = genr_expect_len
    g_dict = {}
    scores = []
    for n in ag.nodes_iter():
        score = ag.node[n]['score']
        scores.append(score)
        g_dict[n] = {'score': score}

    df_pr = pd.DataFrame.from_dict(g_dict, orient='index')
    #df_pr.to_csv()
    gnz = str(generation).zfill(zgnrt)
    genez = str(gn).zfill(zpool)
    it  = str(runned).zfill(ziter)
    filename = set_dir+'/pr_res_iter/gnrt_'+gnz+'/res_gn'+genez+'_iter'+it+'.csv'
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    df_pr.to_csv(filename, index_label='node')

def save_pr(ag, set_dir, generation, gn, pool_size, genr_expect_len):
    zpool = len(str(pool_size))
    zgnrt = genr_expect_len
    g_dict = {}
    scores = []
    for n in ag.nodes_iter():
        score = ag.node[n]['score']
        scores.append(score)
        g_dict[n] = {'score': score}

    df_pr = pd.DataFrame.from_dict(g_dict, orient='index')
    #df_pr.to_csv()
    gnz = str(generation).zfill(zgnrt)
    genez = str(gn).zfill(zpool)
    filename = set_dir+'/pr_res/gnrt_'+gnz+'/res_gn'+genez+'.csv'
    dirname = os.path.dirname(filename)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    df_pr.to_csv(filename, index_label='node')

def run_a_chromosome(this_setup):
    g2 = this_setup['g']
    #ga_op = this_setup['ga_op']
    generation = this_setup['generation']
    gn = this_setup['gn']
    set_dir = this_setup['set_dir']
    genr_expect_len = this_setup['genr_expect_len']
    max_PRiter = this_setup['max_PRiter']
    pool_size = this_setup['pool_size']
    save_iter = this_setup['save_iter']
    run_PR(g2, set_dir, generation, gn, genr_expect_len, max_PRiter, pool_size, save_iter)
    g2 = None

def run_a_generation(ga_op, base, ag, generation, pool_size, run_pool=False):
    #st = 0
    g = initialize_PR(ag)
    base.update(dict(generation=generation, gn=None))
    input_0 = list(range(pool_size))
    inputs = []
    for i in input_0:
        b = copy.deepcopy(base)
        b['gn'] = i
        chrom = ga_op.get_sol(generation, i)
        g2 = ga_op.set_attrs(copy.deepcopy(g), chrom)
        b['g'] = g2
        b['gn'] = i
        inputs.append(b)

    if run_pool:
        cpupoolsize = multiprocessing.cpu_count()
        pool = multiprocessing.Pool(processes=cpupoolsize, initializer=start_process,)
        pool.map(run_a_chromosome, inputs)
        pool.close()
        pool.join()
    else:
        for i in range(len(inputs)):
            this_setup = inputs[i]
            run_a_chromosome(this_setup)
