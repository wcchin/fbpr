# -*- coding: UTF-8 -*-

import os
import pandas as pd
from scipy import stats

"""
def get_flow(flowsf, flows_fs=None):
    if flows_fs is None:
        flows_fs = { 'node':'node', 'volume':'flows' }
    df_vol = pd.read_csv(flowsf)
    df_vol.rename(columns={ v:k for k,v in flows_fs.iteritems() })
    return df_vol
"""

def get_fitness_ofa_generation(generation, set_dir, flows_df, genr_expect_len):
    #g = getnet.create_graph()
    #vols = get_flow(flowsf, flows_fs=flows_fs)
    vols = flows_df
    gnz = str(generation).zfill(genr_expect_len)

    res_path = set_dir+'/pr_res/gnrt_'+gnz
    pr_res =  os.listdir(res_path)
    fit_dict = {}
    for r in pr_res:
        f_key = r[6:-4]
        #print f_key
        fname = res_path+'/'+r
        scores = pd.read_csv(fname, index_col=0)
        merged = pd.merge(vols, scores, how='inner', left_index=True, right_index=True)
        #print len(merged), len(vols), len(scores)
        #print merged.head()
        s = merged.score.tolist()
        v = merged.volume.tolist()
        fn,pv = stats.spearmanr(s, v, axis=0)
        fit_dict[f_key] = {'rho':fn, 'pval':pv}
    fit_df = pd.DataFrame.from_dict(fit_dict, orient='index')
    fit_df = fit_df[['rho','pval']]
    output = set_dir+'/fitness/gnrt_'+gnz+'.csv'
    #output = 'pr_results/set_'+sz+'/fitness_gnrt_'+gnz+'.csv'
    dirname = os.path.dirname(output)
    if not os.path.exists(dirname):
        os.makedirs(dirname)
    fit_df.to_csv(output, index_label='gene_no')

if __name__ == '__main__':
    get_fitness_ofa_generation(0)
