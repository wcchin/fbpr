# -*- coding: UTF-8 -*-

import csv
import pandas as pd
#import numpy as np
import networkx as nx

def file_read(intersf, inters_fs=None):
    if inters_fs is None:
        inters_fs = { 'node1':'node1', 'node2':'node2' }
    df = pd.read_csv(intersf)
    os = df[inters_fs['node1']].tolist()
    ds = df[inters_fs['node2']].tolist()
    links = list(zip(os,ds))
    return links

def create_graph(links, attrlist=None):

    ag = nx.DiGraph()
    """
    for i in range(5527):
        n = i + 1
        g.add_node(n)
    print g.number_of_nodes()
    """

    ### using WHO as node id
    #links = file_read()
    for o,d in links:
        #o = int(o)
        #d = int(d)
        ag.add_edge(o,d)
        ag.add_edge(d,o)
    #print ag.number_of_nodes()
    #print ag.number_of_edges()

    #set_attrs(ag)
    nodes_list = nx.nodes(ag)
    return ag, nodes_list
"""
def set_attrs(ag, attrlist=None):
    if attrlist is None:
        for n in ag.nodes_iter():
            ag.node[n]['attr'] = 1
            ag.node[n]['out_sum'] = 0
    else:
        for n in ag.nodes_iter():
            ag.node[n]['attr'] = attrlist[n]
            ag.node[n]['out_sum'] = 0
    get_link_weight(ag)
    return ag

def get_link_weight(ag):
    for n in ag.nodes_iter():
        out = ag[n]
        #print n, out
        suma = 0
        for d in out:
            suma = suma + ag.node[d]['attr']
        ag.node[n]['out_sum'] = suma

    for o,d in ag.edges_iter():
        da = ag.node[d]['attr']
        os = ag.node[o]['out_sum']
        #ag.node[o]['out_sum'] = ag.node[o]['out_sum'] + da
        ag.edge[o][d]['link_weight'] = float(da)/float(os)
        #print ag.edge[o][d]['link_weight']
        #print ag.edge[o][d]['link_weight']<=1
"""

if __name__ == '__main__':
    g = create_graph()
    set_attrs(g)
    #links = file_read('intersect1.txt')
    """
    #print g[0]
    import pickle
    import matplotlib.pyplot as plt
    with open('pos_file.txt', 'rb') as handle:
        pos = pickle.loads(handle.read())
    #print pos[0]
    nx.draw_networkx_edges(g, pos=pos)
    plt.show()
    """
    """
    with open('intersect1.csv', 'wb') as f:
        writer = csv.writer(f)
        for l in links:
            writer.writerow(l)
    """
