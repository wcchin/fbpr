# -*- coding: UTF-8 -*-

import os
import pandas as pd
from scipy import stats
from random import uniform, randint, random
import csv
import numpy as np

#import get_sol as getsol

def process_GA(ga_op, generation, set_dir, genr_expect_len, volrange):
    #sz = str(setno).zfill(3)
    gnz = str(generation).zfill(genr_expect_len)
    df_fit = read_fitness(set_dir, gnz)

    ## selecting genes
    selected_genes, elites = selection(df_fit)

    ## setting up mating pool
    mating_pool = get_genes(ga_op, selected_genes, generation)
    elites_gene = get_genes(ga_op, elites, generation)
    #for g in mating_pool:
    #    print g
    ## generating offsprings
    off_springs = elites_gene
    while len(off_springs)<len(selected_genes):
        a = randint(0,len(mating_pool)-1)
        b = randint(0,len(mating_pool)-1)
        while b==a:
            b = randint(0,len(mating_pool)-1)
        off1,off2 = generate_offspring(mating_pool[a],mating_pool[b], volrange)
        off_springs.append(off1)
        off_springs.append(off2)
    #print len(off_springs)

    ## export to file
    new_gen = generation+1
    ngnz = str(new_gen).zfill(genr_expect_len)
    filename = set_dir+'/sols/gnrt_'+ngnz+'.csv'
    with open(filename, 'wb') as f:
        writer = csv.writer(f)
        #print off_springs
        for l in off_springs:
            #print l
            writer.writerow(l)

def read_fitness(set_dir, gnz):
    fitnessf = set_dir+'/fitness/gnrt_'+gnz+'.csv'
    df_fit = pd.read_csv(fitnessf)
    return df_fit

def selection(df_fit):
    #print df_fit.head()
    eliteno = 2
    genes = df_fit.gene_no.tolist()
    rhos = df_fit.rho.tolist()
    sorted_genes = [y for x,y in sorted(zip(rhos,genes), reverse=True)]
    elites = sorted_genes[0:eliteno]
    #pseudo_rhos = [ abs(r) for r in rhos ]
    pseudo_rhos = []
    for r in rhos:
        if r>0:
            pseudo_rhos.append(r)
        else:
            pseudo_rhos.append( abs(r)/100.0 )
    cum_pseudo_rhos = []
    cum = 0.0
    for r in pseudo_rhos:
        cum = cum + r
        cum_pseudo_rhos.append( cum )
    #print len(cum_pseudo_rhos)
    #print len(pseudo_rhos)
    #print pseudo_rhos
    sumr = sum(pseudo_rhos)
    #print sumr
    selected_gene = []
    for i in range(50):
        rand = uniform(0.0, sumr)
        j = 0
        while rand>cum_pseudo_rhos[j]:
            j = j + 1

        this_gene = genes[j]
        selected_gene.append(this_gene)
    #print sorted(selected_gene)
    return selected_gene, elites

def get_genes(ga_op, selected_genes, generation):
    #candidates = list(set(selected_genes))
    #parent_sols_file = 'solutions/sol_'+sz+'_'+gnz+'.csv'
    mating_pool = []
    for gn in selected_genes:
        agene = ga_op.get_sol(generation,gn)
        mating_pool.append(agene)
    return mating_pool

def generate_offspring(agene, bgene, volrange):
    lengene = len(agene)
    vmin, vmax = volrange
    if agene!=bgene:
        off1 = []
        off2 = []
        if random()<=0.05:
            sol_no = 2
            sols0 = list(np.random.randint(vmin, vmax, size=(sol_no,lengene)))
            sols = [list(s) for s in sols0]
            off1 = sols[0]
            off2 = sols[1]
        else:
            try:
                crossover = []
                for i in range(lengene):
                    crossover.append(randint(0,1))
                for i in range(len(crossover)):
                    c = crossover[i]
                    if c==0:
                        off1.append(agene[i])
                        off2.append(bgene[i])
                    else:
                        off1.append(bgene[i])
                        off2.append(agene[i])
            except:
                print len(agene)
                print len(bgene)
    else:
        off1 = agene
        sol_no = 1
        sols0 = list(np.random.randint(vmin, vmax, size=(sol_no,lengene)))
        sols = [list(s) for s in sols0]
        off2 = sols[0]
    return off1,off2
