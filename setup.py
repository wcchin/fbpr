from setuptools import setup
from setuptools import find_packages

setup(
    name="FBPR",

    version="0.1.0",
    
    author="Benny Chin",
    author_email="wcchin.88@gmal.com",

    packages=['fbpr', 'fbpr.test_data'],

    include_package_data=True,

    url="https://bitbucket.org/wcchin/fbpr",

    license="LICENSE.txt",
    description="An algorithm that calibrate the PR score to meet the flow, with the optimal weight.",

    long_description=open("README.md").read(),
    
    classifiers=[
        'Development Status :: 3 - Alpha',

        'Intended Audience :: Science/Research',
        'Topic :: Scientific/Engineering :: GIS',

         'License :: OSI Approved :: MIT License',

        'Programming Language :: Python :: 2.7',
    ],

    keywords='flow-based, Weighted PageRank',

    install_requires=[
        "numpy",
        "scipy", 
        "pandas",
        "geopandas",
        "shapely",
        "jinja2",
        "descartes",
        "matplotlib",
        "seaborn",
        "pyshp",
    ],
)
