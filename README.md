
# Flow-based PageRank algorithm
An algorithm that calibrate the PR score to meet the flow, with the optimal weight.

more details:
### article
Wen, T. H., Chin, W. C. B., Lai, P. C. (2017). Understanding the topological characteristics and flow complexity of urban traffic congestion. Physica A: Statistical Mechanics and its Applications 473: 166-177. DOI http://dx.doi.org/10.1016/j.physa.2017.01.035

<a href="http://wcchin.github.io/publications/PHYSA_166902/" target="blank">abstract and more links</a>

### book chapter
Wen, T. H., Chin, W. C. B. & Lai, P. C. (2017). Link structure analysis of urban street networks for delineating traffic impact areas. In M. Nemiche, M. Essaaidi (eds.), Advances in Complex Societal, Environmental and Engineered Systems, Nonlinear Systems and Complexity 18. Part 2: 203-220. DOI 10.1007/978-3-319-46164-9_10, ISBN 978-3-319-46164-9.

Link: <a href="http://wcchin.github.io/publications/BookChap_ComplexSocietalSystem/" target="blank">abstract and more links</a>

=============================

after download and unzip the repo:

```sh
cd the_directory
pip install .
```

=============================

using this package

import it
```python 
from fbpr import FBPR
```

setup the file and file setup
```python
intersf = 'inter_TP_1m.csv'
flowsf = 'flow_TP_1m.csv'
inters_fs = { 'node1':'n1', 'node2':'n2' }
flows_fs = {'node':'rid', 'volume':'flow'}
```

initializing the FBPR
```python
fbpr = FBPR(intersf, flowsf, inters_fs=inters_fs, flows_fs=flows_fs)
```

initialize the solutions sets, run once
```python
fbpr.init_solutions(pool_size=3, set_count=3)
```

run all sets
```python
fbpr.run_all(max_generation=20, max_PRiter=500, run_pool=True, save_iter=False)
```

run one set (e.g. first set)
```python
run_set(0, max_generation=20, max_PRiter=500, run_pool=True, save_iter=False)
```


```python
fbpr.get_optimal_sol(export=True) ## export the dataframe

optimal_sol_df = fbpr.get_optimal_sol(export=False) ## return the dataframe
```


check the performance of each set
```python
fbpr.ga_performance(set_no=0)
fbpr.ga_performance(set_no=1)
fbpr.ga_performance(set_no=2)
```

=============================

### License
MIT, please check the license file.
